import React from 'react';

const API = 'https://printful.com/test-quiz.php?action=';

class Answers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showButton: false,
            questions: [],
            answers: [],
            answered: [],
        };
        
        this.checkAnswer = this.checkAnswer.bind(this);
        this.getAnswers = this.getAnswers.bind(this);
        this.getQuestions = this.getQuestions.bind(this);
        this.nextQuestion = this.nextQuestion.bind(this);
        this.handleShowButton = this.handleShowButton.bind(this);
    }
    
    checkAnswer(e) {
            this.props.answered[this.props.nr] = this.state.answers[e];
            this.handleShowButton();
    }
    getQuestions(action = 'questions&quizId='+ this.props.quizId){
        fetch(API+action)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    questions: data,
                    questionId: data[this.props.nr].id,
                    nr: this.props.nr,
                    total: data.length
                });
                this.getAnswers();
            })
            .catch(error => console.log(error))
    }

    getAnswers(action = 'answers&quizId='+ this.props.quizId +'&questionId='+this.state.questions[this.state.nr].id){
        fetch(API+action)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    answers: data,
                    nr: this.state.nr + 1,
                });
            })
            .catch(error => console.log(error))
    }

    nextQuestion(){
        const {nr, total} = this.state;

        if (nr === total) {
            this.props.handleToScore();
        } else {
            this.getAnswers();
            this.props.nextQuestion();
            this.setState({
                showButton: false,
            })
        }
    }

    handleShowButton() {
        this.setState({
            showButton: true,
            questionAnswered: true
        })
    }

    componentDidMount(){
        this.getQuestions();
    }
    
    render() {
        const { showButton } = this.state;

        const items = this.state.answers.map((item, key) =>
            <li onClick={() => this.checkAnswer(key)} value={item.id} key={item.id}><p>{item.title}</p></li>
        );

        const style = {
            width: Math.round((100 * this.props.nr+1) / this.state.total)+'%',
        };
        return (
            <div id="answers">
                <ul>
                    {items}
                </ul>
                <div id="progressbar">
                    <div style={style}></div>
                </div>
                <div id="submit">
                    {showButton ? <button className="fancy-btn"
                                          onClick={this.nextQuestion}>{this.props.nr+1 === this.state.questions.length ? 'Finish quiz' : 'Next question'}</button> : null}
                </div>
            </div>
        );
    }
}

export default Answers