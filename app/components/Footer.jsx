import React from 'react'

class Footer extends React.Component{
    render() {
        return (
            <footer>
                <small>
                    This is a small Quiz application.
                </small>
                <p>created by Deniss Arefjevs</p>
            </footer>
        )
    }
}

export default Footer