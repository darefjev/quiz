import React from 'react';
import Answers from 'Answers';
import Results from 'Results';
import Footer from 'Footer';

const API = 'https://printful.com/test-quiz.php?action=';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nr: 0, // current question
            step: 0, // current layout, 0 - start, 1 - quiz, 3 - score
            name: '', //user name
            total: 0,  // questions total
            showButton: false,
            validation: true,
            quizList: [],
            currentQuiz: 0,  // quiz id
            questions: [],
            answered: [],
        };
        this.getQuestions = this.getQuestions.bind(this);
        this.nextQuestion = this.nextQuestion.bind(this);
        this.handleShowButton = this.handleShowButton.bind(this);
        this.handleToQuiz = this.handleToQuiz.bind(this);
        this.handleToScore = this.handleToScore.bind(this);
        this.handleToStart = this.handleToStart.bind(this);
    }

    getQuestions(action) {
        fetch(API + action)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    questions: data,
                    total: data.length
                });
            })
            .catch(error => console.log(error))
    }

    pushData(nr) {
        this.setState({
            question: this.state.questions[nr],
            nr: this.state.nr + 1,
        });
    }

    componentDidMount(action = 'quizzes') {
        fetch(API + action)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    quizList: data,
                //    currentQuiz: data[0].id
                });
            })
            .catch(error => console.log(error))
    }

    nextQuestion() {
        let {nr, total} = this.state;
        if (nr === total) {
            this.setState({
                step: 3
            });
        } else {
            this.pushData(nr);
            this.setState({
                showButton: false,
                questionAnswered: false
            });
        }
    }

    handleShowButton() {
        this.setState({
            showButton: true,
            questionAnswered: true
        })
    }

    handleToScore() {
        this.setState({
            step: 3
        });
    }

    handleToStart() {
        this.setState({
            step: 0,
            nr: 0,
            currentQuiz: 0,
        });
    }

    handleToQuiz() {
        if (this.state.name === '' || this.state.currentQuiz === 0) {
            this.setState({
                validation: false,
            });
        } else {
            this.getQuestions('questions&quizId=' + this.state.currentQuiz);
            this.setState({
                step: 1,
                validation: true,
            });
        }
    }

    handleInputChange(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleChange(e) {
        this.setState({
            currentQuiz: e.target.value
        });
    }

    render() {
        let {nr, total, validation, currentQuiz } = this.state;

        const items = this.state.quizList.map((item) =>
            <option value={item.id} key={item.id}>{item.title}</option>
        );

        if (this.state.step === 0) {
            return (
                <div className="container">
                    <div className="form">
                        <h2>Technical Task</h2>
                        {validation ===false ? <label className="error">Field Name is required or test are not chosen</label> : null}
                        <input value={this.state.name} onChange={(e) => this.handleInputChange(e)}
                               placeholder="Enter your Name"/>
                        <select onChange={this.handleChange.bind(this)}>
                            <option value="0">Choose test</option>
                            {items}
                        </select>
                        <button className="fancy-btn" onClick={this.handleToQuiz}>
                            Start
                        </button>
                    </div>
                    <Footer/>
                </div>
            );
        } else if (this.state.step === 3) {
            return (
                <div className="container">
                    <Results  quizId={currentQuiz}
                              answered={this.state.answered}
                              name={this.state.name}
                              total={this.state.total}
                              handleToStart={this.handleToStart} />
                    <Footer/>
                </div>
            );
        } else {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-lg-10 col-lg-offset-1">
                            <div id="question">
                                <h4>Question {nr + 1}/{total}</h4>
                                <p>{this.state.questions[nr] ? this.state.questions[nr].title : ''}</p>
                            </div>
                            <Answers total={total} answered={this.state.answered} answers={this.state.answers}
                                     quizId={currentQuiz} nr={nr} question={this.state.question}
                                     questions={this.state.questions}
                                     showButton={this.handleShowButton}
                                     nextQuestion={this.nextQuestion}
                                     handleToScore={this.handleToScore} />
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        }
    }
}

export default Main
