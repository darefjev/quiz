import React from 'react';

const API = 'https://printful.com/test-quiz.php?action=';

class Results extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toStart: false,
        };
        this.getAnswers = this.getAnswers.bind(this);
        this.handleToStart = this.handleToStart.bind(this);
    }

    getAnswers(action){
        fetch(API+action)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    answers: data,
                    correct: data.correct,
                    total: data.total,
                    toStart: true,
                });
            })
            .catch(error => console.log(error))
    }

    handleToStart(){
        this.setState({
            toStart:false,
        });
        this.props.handleToStart();
    }

    componentDidMount(){
        let action = 'submit&quizId='+ this.props.quizId;

        const items = this.props.answered.map((item) =>
            action = action+'&answers[]='+item.id
        );

        this.getAnswers(action);
    }

    render() {
        let {toStart } = this.state;

        return (
            <div className="form">
                <h2>Thanks, user {this.props.name}</h2>

                <h4>You responded correctly to {this.state.correct} out of {this.state.total} questions</h4>

                <div id="submit">
                    {toStart ? <button className="fancy-btn" onClick={this.handleToStart}>Restart</button> : null}
                </div>
            </div>
        );
    }
}

export default Results;