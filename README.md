# Quiz Application built with react
This is a test task application 

Working with Printful quiz API https://bit.ly/2UxVDMI

Architecture:
4 components: <br/>
Main - where shows all another components and load quiz list form API <br/>
Answers - where getting answers for questions from API   <br/>
Results - show result <br/>
Footer - show footer component

Deploying the Project

To install npm install
To build the project code, run <strong>npm run build</strong>. <br/>
Running <strong>npm start</strong> can be used to start the production app